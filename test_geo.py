
import pytest
from floodsystem.geo import stations_by_distance, stations_within_radius, rivers_with_station, stations_by_river, rivers_by_station_number
from floodsystem.stationdata import build_station_list
from haversine import haversine

def test_stations_by_distance():
    """
    Test stations by distance
    """
    stations = build_station_list() # build list of stations
    station_dist_list = stations_by_distance(stations, (52.2053, 0.1218))
    assert station_dist_list[0] == ('Cambridge Jesus Lock', 'Cambridge', 0.8402364350834995) # check output matches given input
    
def test_stations_within_radius():
    """
    Test stations within radius
    """
    stations = build_station_list() # builf list of stations
    stations_within_rad = stations_within_radius(stations, (52.2053, 0.1218), 10)
    assert stations_within_rad[0] == 'Bin Brook'
    
def test_rivers_with_station():
    """
    Test rivers with station
    """
    stations = build_station_list() # builf list of stations
    rivers = rivers_with_station(stations)
    assert list(rivers)[0:2] == ['Addlestone Bourne', 'Adur']
    
def test_stations_by_river():
    """
    Test stations by river
    """
    stations = build_station_list() # builf list of stations
    riverdictionary = stations_by_river(stations)
    stationlist = []
    for station in riverdictionary['River Cam']:
        stationlist.append(station.name)
    assert sorted(stationlist) == ['Cam', 'Cambridge', 'Cambridge Baits Bite', 'Cambridge Jesus Lock', 'Dernford', 'Weston Bampfylde']
    
def test_rivers_by_station_number():
    """
    Test rivers by station number
    """
    stations = build_station_list() # builf list of stations
    riverstationdictionary = rivers_by_station_number(stations, 2)
    assert riverstationdictionary == [('Thames', 55), ('River Great Ouse', 31)]
    
    
    
               