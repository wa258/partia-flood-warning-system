"""Unit test for the station module"""

import pytest
from station import MonitoringStation, typical_range_consistent, inconsistent_typical_range_stations

def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_typical_range_consistent():

    # Create a station for TRUE case
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = ('x', 'y')
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert typical_range_consistent(s) == True

    # Create a station for NONE case
    s_id1 = "test-s-id"
    m_id1 = "test-m-id"
    label1 = "some station"
    coord1 = ('x', 'y')
    trange1 = (None)
    river1 = "River X"
    town1 = "My Town"
    a = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)
    
    assert typical_range_consistent(a) == False

    # Create a station for FALSE case
    s_id2 = "test-s-id"
    m_id2 = "test-m-id"
    label2 = "some station"
    coord2 = ('x', 'y')
    trange2 = (2.3, -3.4445)
    river2 = "River X"
    town2 = "My Town"
    b = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)
    
    assert typical_range_consistent(b) == False

def test_inconsistent_typical_range_stations():
    
    s_id2 = "test-s-id"
    m_id2 = "test-m-id"
    label2 = "some station"
    coord2 = ('x', 'y')
    trange2 = (2.3, -3.4445)
    river2 = "River X"
    town2 = "My Town"
    a = MonitoringStation(s_id2, m_id2, label2, coord2, trange2, river2, town2)
    
    s_id1 = "test-s-id"
    m_id1 = "test-m-id"
    label1 = "some station"
    coord1 = ('x', 'y')
    trange1 = (None)
    river1 = "River X"
    town1 = "My Town"
    b = MonitoringStation(s_id1, m_id1, label1, coord1, trange1, river1, town1)
    
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = ('x', 'y')
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    c = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    
    
    s = inconsistent_typical_range_stations([a,b,c])
    assert s == [a,b]