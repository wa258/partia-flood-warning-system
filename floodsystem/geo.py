"""This module contains a collection of functions related to
geographical data.

"""

#import utils
#from .utils import sorted_by_key
from floodsystem.utils import sorted_by_key
 #given at the first place
 
from haversine import haversine 
 #import haversine
 
 #task 1B
def stations_by_distance(stations, p): 
    stations_by_distance_list = [] #list
    for station in stations: #can run through every station
        stations_by_distance_list.append((station.name, station.town, haversine(p,station.coord)))
    stations_by_distance_list1 = sorted_by_key(stations_by_distance_list, 2) #sorted tuple
    return stations_by_distance_list1 #print
    
 #task 1C
def stations_within_radius(stations, centre, r):
    name = [] #create a list     
    for station in stations:
        if haversine(centre,station.coord) <= r:
            name.append(station.name) #add up
    name_1 = sorted(name)
    return name_1

#Task 1D
def rivers_with_station(stations):
   
    rivers = set() #"create an empty set"
 
    for station in stations: #"for each station in the list of stations, extract the river"
        rivers.add(station.river) #"add into empty set"
    return(sorted(rivers))
        

def stations_by_river(stations):
    
    stationriver = {} #"create empty dictionary"

    for station in stations: 
        if (station.river) in stationriver:
            stationriver[station.river].append(station) #"if river of station is in the dictionary, add to the existing dictionary"
        else:
            stationriver[station.river] = [station] #"if not shared with dictionary, ignore"

    return stationriver
    
def rivers_by_station_number(stations, N):
    riversbystation = {} #create empty dictionary
    
    for station in stations:
        if (station.river) in riversbystation:
            riversbystation[station.river] += 1 #if river is common, add 1
        else:
            riversbystation[station.river] = 1 #if not, create new river set
    riverlist = list(riversbystation.items()) #change to list
    riverlist.sort(key = lambda tup: tup [1], ) # sort based on number, not the name
    riverlist.reverse()
    
    
    results = [] #create empty list

    while N>0:
        count = 1 #set count as 1
        while (count != 0):
            if riverlist[count + len(results) - 1][1] == riverlist[count + len(results)][1]:
                count += 1
            else: #when there are no more rivers that share the same last value
                for i in range(count):
                    results.append(riverlist[len(results)]) #add additional rivers that share same value
                N -= count
                count = 0
                
    return results
        

