"""This module contains a collection of functions related to
geographical data.

"""

#import utils
#from .utils import sorted_by_key
from floodsystem.stationdata import build_station_list

def rivers_with_station(stations):
   
    rivers = set() #"create an empty set"
 
    for station in stations: #"for each station in the list of stations, extract the river"
        rivers.add(station.river) #"add into empty set"
    return(sorted(rivers))
        

def stations_by_river(stations):
    
    stationriver = {} #"create empty dictionary"

    for station in stations: 
        if (station.river) in stationriver:
            stationriver[station.river].append(station) #"if river of station is in the dictionary, add to the existing dictionary"
        else:
            stationriver[station.river] = [station] #"if not shared with dictionary, ignore"

    return stationriver
    
def rivers_by_station_number(stations, N):
    riversbystation = {} #create empty dictionary
    
    for station in stations:
        if (station.river) in riversbystation:
            riversbystation[station.river] += 1 #if river is common, add 1
        else:
            riversbystation[station.river] = 1 #if not, create new river set
    riverlist = list(riversbystation.items()) #change to list
    riverlist.sort(key = lambda tup: tup [1], ) # sort based on number, not the name
    riverlist.reverse()
    
    
    results = [] #create empty list

    while N>0:
        count = 1 #set count as 1
        while (count != 0):
            if riverlist[count + len(results) - 1][1] == riverlist[count + len(results)][1]:
                count += 1
            else: #when there are no more rivers that share the same last value
                for i in range(count):
                    results.append(riverlist[len(results)]) #add additional rivers that share same value
                N -= count
                count = 0
                
    return results
            
    
        

